char mapLetter(int letter);

String polishLetters(String polish){
  int prenumber = 0;
  Serial.println(polish.length());
  for(int i = 0; i < polish.length(); i++){
    if(polish[i] >= 192){
        prenumber = polish[i] << 8;
        polish[i] = 0;
        continue;
    } 
    if(prenumber != 0){
        polish[i] = mapLetter((int)polish[i] + prenumber);
        prenumber = 0;  
    }
  }
  return polish;
}

char mapLetter(int letter) {
    switch(letter){
      case 50564: // 50564 ń
        return 'n';
      case 50361: // 50563 Ń
        return 'N'; 
      case 50309: // 50309 ą
        return 'a';
      case 50308: // 50308 Ą
        return 'A';
      case 50620: // 50620 ż
      case 50618: // 50617 ź
        return 'z';
      case 50619: // 50619 Ż
      case 50617: // 50617 Ź
        return 'Z';
      case 50311: // 50311 ć
        return 'c';
      case 50310: // 50310 Ć
        return 'C'; 
      case 50587: // 50587 ś
        return 's'; 
      case 50586: // 50586 Ś
        return 'S'; 
      case 50099: // 50099 ó
        return 'o'; 
      case 50067: // 50067 Ó
        return 'O';
      case 50329: // 50329 ę
        return 'e'; 
      case 50328: // 50328 Ę
        return 'E';
      case 50562: // 50562 ł
        return 'l'; 
      case 50561: // 50561 Ł
        return 'L';
      case 57984:
        return '-';
      default: 
        return letter;
    }
}
