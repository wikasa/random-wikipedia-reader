#include <SoftwareSerial.h>
#include <BearSSLHelpers.h>
#include <CertStoreBearSSL.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiGratuitous.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiSTA.h>
#include <ESP8266WiFiType.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiClientSecureAxTLS.h>
#include <WiFiClientSecureBearSSL.h>
#include <WiFiServer.h>
#include <WiFiServerSecure.h>
#include <WiFiServerSecureAxTLS.h>
#include <WiFiServerSecureBearSSL.h>
#include <WiFiUdp.h>
#include <ESP8266HTTPClient.h>
#include "PolishLetters.h"
#include <TFT_eSPI.h>
#include <SPI.h>
#include <ESP8266WiFi.h>

TFT_eSPI tft = TFT_eSPI();
uint16_t i = 0;
char* ssid = "hotspot";
char* password = "hotspot123";
const char * headerKey[] = {"location"};
const size_t numberOfHeaders = 1;
String headerLocation = " ";
String wikipedia = " ";
String title = " ";
String summary = " ";
short pages = 1;
short currentPage = 1;
String summaryCurrentPage = " ";
const int INTERVAL = 1000 * 60 * 60 * 24; // ms * sec * min * hr
unsigned long lastTime = 0;

HTTPClient http;

void setWIFI() {
  WiFi.begin(ssid, password);
  tft.setFreeFont(&FreeMono9pt7b);
  tft.print("Laczenie");
  while(WiFi.status() != WL_CONNECTED){
    delay(100);
    tft.print(".");
  }
  tft.println();
  tft.print("Polaczono IP: ");
  tft.println(WiFi.localIP());
}

void fetchWikipedia() {
  std::shared_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
  client->setInsecure();
  if (!http.begin(*client, "https://pl.wikipedia.org/api/rest_v1/page/random/summary")) {
    return;
  }
  http.collectHeaders(headerKey, numberOfHeaders);
  int code = http.GET();
  Serial.println(code);
  if (code > 0) {
    if (code == 303) {
      headerLocation = http.header("location");
      Serial.println(headerLocation);
      headerLocation.replace("..", "https://pl.wikipedia.org/api/rest_v1/page");
      Serial.println(headerLocation);
    }
  }
  http.end();
  if(!http.begin(*client, headerLocation)){
    return;
  }
  code = http.GET();
  Serial.println(code);
  if (code > 0) {
    if (code == 200) {
      wikipedia = http.getString();
      Serial.println(wikipedia);

      String extract = wikipedia;
      Serial.println(extract);
      int indexOfExtract = extract.indexOf("\"extract\":\"");
      Serial.println(indexOfExtract);
      String partOneExtract = extract.substring(indexOfExtract + 11);
      Serial.println(partOneExtract);
      indexOfExtract = partOneExtract.indexOf("\",\"extract_html\"");
      Serial.println(indexOfExtract);
      String partTwoExtract = partOneExtract.substring(0, indexOfExtract);
      Serial.println(partTwoExtract);
      String partSummaryTitle = partTwoExtract;
      int indexOfTitle = partSummaryTitle.indexOf(" – ");
      if (indexOfTitle == -1) {
        indexOfTitle = partSummaryTitle.indexOf(" - ");
      }
      if (indexOfTitle != -1) {
        summary = partTwoExtract.substring(indexOfTitle + 5);
        char firstLetter = summary[0] - 'a' + 'A';
        Serial.println(firstLetter);
        summary[0] = firstLetter;
        Serial.println(summary);
      }
      else {
        summary = partTwoExtract;
      }
      Serial.println(indexOfTitle);


      String partOneTitle = wikipedia;
      Serial.println(partOneTitle);
      indexOfTitle = partOneTitle.indexOf("\"displaytitle");
      Serial.println(indexOfTitle);
      String partTwoTitle = partOneTitle.substring(0, indexOfTitle - 2);
      Serial.println(partTwoTitle);
      indexOfTitle = partTwoTitle.indexOf("\"title\":\"");
      Serial.println(indexOfTitle);
      String titleInPolish = partTwoTitle.substring(indexOfTitle + 9);
      title = polishLetters(titleInPolish);
      Serial.println(title);
      short titleLines = ceil(title.length()/23.0);
      Serial.println(titleLines);
      short maxTextLines = floor(13-1.5*titleLines);
      short textLines = ceil(summary.length()/35.0);
      pages = ceil((float)textLines/maxTextLines);
      currentPage = 1;
      int maxLettersPerPage = 35.0*maxTextLines;
      summaryCurrentPage = summary.substring(maxLettersPerPage*(currentPage-1), maxLettersPerPage*currentPage);
      Serial.println(maxTextLines);
      Serial.println(textLines);
      Serial.println(pages);
      Serial.println(maxLettersPerPage);
      Serial.println(maxLettersPerPage*(currentPage-1));
      Serial.println(maxLettersPerPage*currentPage);
      Serial.println(summaryCurrentPage);
      summaryCurrentPage = polishLetters(summaryCurrentPage);
      Serial.println("summary z polskimi znakami " + summary);
      Serial.println("summary bez polskich znakow " + summary);
      
      tft.fillScreen(0xFFFF);
      tft.setCursor(0, 20);
      tft.setFreeFont(&FreeMono18pt7b);
      tft.println(title);
      tft.setFreeFont(&FreeMono12pt7b);
      tft.println(summaryCurrentPage);
    }
  }
  http.end();
}
void nextPage() {
  currentPage = currentPage+1;
  if(currentPage > pages){
    currentPage = 1;
  }

   short titleLines = ceil(title.length()/23.0);
      Serial.println(titleLines);
      short maxTextLines = floor(13-1.5*titleLines);
      short textLines = ceil(summary.length()/35.0);
      int maxLettersPerPage = 35.0*maxTextLines;
      summaryCurrentPage = summary.substring(maxLettersPerPage*(currentPage-1), maxLettersPerPage*currentPage);
      Serial.println(maxTextLines);
      Serial.println(textLines);
      Serial.println(pages);
      Serial.println(maxLettersPerPage);
      Serial.println(maxLettersPerPage*(currentPage-1));
      Serial.println(maxLettersPerPage*currentPage);
      Serial.println(summaryCurrentPage);
      summaryCurrentPage = polishLetters(summaryCurrentPage);
      Serial.println("summary z polskimi znakami " + summary);
      Serial.println("summary bez polskich znakow " + summary);
      
      tft.fillScreen(0xFFFF);
      tft.setCursor(0, 20);
      tft.setFreeFont(&FreeMono18pt7b);
      tft.println(title);
      tft.setFreeFont(&FreeMono12pt7b);
      tft.println(summaryCurrentPage);
}
void setup() {
  Serial.begin(115200);
  tft.begin(115200);
  tft.init();
  tft.setFreeFont(&FreeMono9pt7b);
  tft.setRotation(3);
  tft.fillScreen(0xFFFF);
  tft.setCursor(0, 10);
  tft.setTextColor(TFT_BLACK, TFT_WHITE);
  tft.fillScreen(0xFFFF);
  setWIFI();
  delay(4000);
  Serial.println("wifi connected");
  fetchWikipedia();
}

void loop() {
  uint16_t x, y;
  if (tft.getTouch(&x, &y)) {
    tft.fillScreen(0xFFFF);
    if(x<240){
      fetchWikipedia();
    }
    else{
      nextPage();
    }
  }
  unsigned long currentTime = millis();
  if (lastTime > currentTime || (currentTime - lastTime) > INTERVAL) {
    fetchWikipedia();
    lastTime = currentTime;
  }
  delay(100);
}
